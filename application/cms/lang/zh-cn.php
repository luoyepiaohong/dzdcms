<?php
return [
	'Category Title' => '栏目标题',
	'Category Description' => '栏目简介',
	'Page Title' => '网页标题',
	'Title set for search engine' => '针对搜索引擎设置的标题',
	'Page Keyword' => '网页关键词',
	'Keywords separated by half comma' => '关键字中间用半角逗号隔开',
	'Page Description' => '网页描述',
	'Page description for search engine settings' => '针对搜索引擎设置的网页描述',
	'Title' => '标题',
	'SEO Keyword' => 'SEO关键词',
	'SEO Description' => 'SEO描述',
	'Content' => '内容',

	'Setting Value' => '配置默认值',

];
